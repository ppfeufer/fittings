��    V      �     |      x  l   y     �     �             .     )   C     m  	   {  
   �     �     �     �     �     �     �     �     �     �     	  	   	     	     (	  
   8	     C	     O	     X	     m	     {	     �	     �	  	   �	     �	  	   �	     �	     �	     �	     �	     
     
     
     -
     6
     ;
     C
     Q
     Z
     l
     �
     �
  
   �
     �
  z   �
     %  	   2     <     I     N     [  
   h     s     �     �     �     �     �     �  �   �  	   �     �     �     �     �     �  	   �       
   	               5     G     V     j     x     �  �  �  �         �     �     �     �  3     7   A     y     �     �  
   �     �     �     �     �               !     ;     B     O     a     u     �     �     �     �     �     �            	   4     >     R     c     t     �     �     �     �     �               '     4     K     Y     q     �     �     �  
   �  �   �     L     c     s     �     �     �     �      �     �  "   �          2     I     b  �        \  #   r     �     �     �     �     �     �     �               .     C     [     u     �     �                :   5      @   	   J   S      K              ?   7   N      8   *          #   2   D                     O   L          V      (       3   I   E             &               G       .         P         ;             M   R   B   H                  1   /   A   C   >   6   U                     <           $   
       T             0                             4       Q   ,       '      !              %                   +   =       "   9          )       F   -    *Note: Fit counts may be inaccurate as fits marked independently of doctrines may be counted more than once. Add Category Add Doctrine Add Fit Add Fitting Are you sure you wish to delete this doctrine? Are you sure you wish to delete this fit? Are you sure? Cargo Bay Categories Category Category Color Category List Category Name Category is public Close Copy Buy All Copy Fit (EFT) Created Current Icon Dashboard Delete Category Delete Doctrine Delete Fit Description Doctrine Doctrine Description Doctrine Fits Doctrine Information Doctrine List Doctrine Name Doctrines Doctrines Assigned Drone Bay Edit Category Edit Doctrine Edit/Update Fit Fighter Bay Fighter Tubes Fit Fit Information Fit List Fits Fitting Fitting Notes Fittings Fittings Assigned Fittings and Doctrines Groups Groups Assigned High Slots Hull If you do not see the ship type that you would like to use for the icon, add a fit that uses that ship type and try again. Last Updated Low Slots Medium Slots Name New Category New Doctrine No - Close No Categories Found No Doctrines Found No Fits Found No High Slot Items No Low Slot Items No Medium Slot Items No Rig Slot Items Only selected groups will have access to fittings and doctrines in this category. If no groups are selected the category will be visible to all with fittings module access. Rig Slots Select Doctrine Icon Select Doctrines Select Fits Select Groups Service Slots Ship Type Ships SubSystems Submit This action is permanent. Toggle navigation Update Fitting View All Categories View All Fits View Category Yes - Delete Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2020-10-21 18:04+0000
Last-Translator: trenus, 2023
Language-Team: Spanish (https://app.transifex.com/alliance-auth/teams/107430/es/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: es
Plural-Forms: nplurals=3; plural=n == 1 ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;
 *Nota: El recuento de equipamientos puede ser inexacto, ya que los equipamientos marcados independientemente de las doctrinas pueden contarse más de una vez. Añadir categoría Añadir doctrina Añadir equipamiento Añadir equipamiento ¿Está seguro de que desea eliminar esta doctrina? ¿Está seguro de que desea eliminar este equipamiento? ¿Estás seguro? Bodega Categorías Categoría Color de la categoría Lista de categorías Nombre de la categoría La categoría es pública Cerrar Copiar Comprar todo Copiar equipamiento (EFT) Creado Icono actual Página principal Eliminar categoría Eliminar la doctrina Eliminar equipamiento Descripción Doctrina Descripción de la doctrina Equipamientos de doctrina Información sobre la doctrina Lista de doctrinas Nombre de la doctrina Doctrinas Doctrinas asignadas Muelle de drones Editar categoía Editar doctrina Editar/Actualizar equipamiento Plataforma para cazas Tubos de Lanzamiento de Cazas Equipamiento Información del equipamiento Lista de equipamientos Equipamientos Equipamiento Notas del equipamiento Equipamientos Equipamientos asignados Equipamientos y doctrinas Grupos Grupos asignados Ranura superior Estructura Si no ves el tipo de nave que te gustaría utilizar para el icono, añade un equipamiento que utilice ese tipo de nave e inténtalo de nuevo. Última actualización Ranura inferior Ranura intermedia Nombre Nueva categoría Nueva doctrina No - Cerrar No se han encontrado categorías No se han encontrado doctrinas No se han encontrado equipamientos Ranura superior vacía Ranura inferior vacía Ranura intermedia vacía Ranura de complemento vacía Sólo los grupos seleccionados tendrán acceso a los equipamientos y doctrinas de esta categoría. Si no se selecciona ningún grupo, la categoría será visible para todos los que tengan acceso al módulo de accesorios. Ranura de complemento Seleccionar el icono de la doctrina Seleccionar doctrinas Seleccionar equipamientos Seleccionar grupos Ranura de servicio Tipo de nave Naves Subsistemas Enviar Esta acción es permanente. Alternar navegación Actualizar equipamiento Ver todas las categorías Ver todos los equipamientos  Ver categoría Sí - Eliminar 